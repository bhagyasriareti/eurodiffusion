input_filepath = r"C:\Users\HP\Desktop\data\input.txt"
filepath = input_filepath
grid_size = 10
maximum_countries = 20
initial_city_balance = 1_000_000
representative_portion = 1_000


def reading_file(filepath):
    with open(filepath, 'r') as file:
        data = file.read()
    return data.split('\n')

def is_valid_country(line):
    args = line.split(' ')
    if len(args) != 5:
        raise Exception("Error at line {%s}: invalid number of tokens" % line)
    for i in range(1, 5):
        if int(args[i]) <= 0 or int(args[i]) >= (grid_size + 1):
            raise Exception("Error at line {%s}: invalid country coordinates" % line)

    country = {
        "name": args[0],
        "southwest_coordinates": {
            "x": int(args[1]),
            "y": int(args[2])
        },
        "northeast_coordinates": {
            "x": int(args[3]),
            "y": int(args[4])
        }
    }
    return country


def is_valid_input():
    cases_list = []

    lines = reading_file(input_filepath)
    line_index = 0
    case = 0
    while line_index < len(lines):
        countries_len = int(lines[line_index])
        if countries_len == 0:
            return cases_list
        if countries_len > maximum_countries or countries_len < 1:
            raise Exception("Error in input for case %i: invalid amount of countries" % (case + 1))
        line_index += 1

        countries_list = []
        for j in range(countries_len):
            ordering_input = is_valid_country(lines[line_index])
            print(ordering_input)
            countries_list.append(ordering_input)
            line_index += 1
        case += 1
        cases_list.append(countries_list)

    return cases_list


print(is_valid_input())
